export type Desktop = {
    id?: number;
    company_id?: number;
    platform: string;
    type: string;
    os: string;
    ip: string;
};

export type Contributor = {
    id?: number;
    company_id?: number;
    firstName: string;
    lastName: string;
    title: string;
    jobTitle: string;
    age: number;
};

export type Company = {
    id?: number;
    business_name: string;
    suffix: string;
    industry: string;
    catch_phrase: string;
    bs_company_statement: string;
    logo: string;
    type: string;
    phone_number: string;
    full_address: string;
    latitude: string;
    longitude: string;
    contributors?: Contributor[];
    desktops?: Desktop[];
};
