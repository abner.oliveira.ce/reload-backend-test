import { Knex } from "knex";

import { companies } from "./data/companies4838";
import { contributors } from "./data/contributors4838";
import { desktops } from "./data/desktops4838";

export async function seed(knex: Knex): Promise<void> {
    await knex("contributors").del();
    await knex("desktops").del();
    await knex("companies").del();

    await knex("companies").insert(companies);
    await knex("contributors").insert(contributors);
    await knex("desktops").insert(desktops);
}
