import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    return await knex.schema.createTable("contributors", function (table) {
        table.increments("id").primary();
        table
            .integer("company_id")
            .unsigned()
            .index()
            .references("id")
            .inTable("companies");
        table.string("firstName", 255).notNullable();
        table.string("lastName", 255).notNullable();
        table.string("title", 255);
        table.string("jobTitle", 255);
        table.integer("age");
        table.timestamp("created_at").defaultTo(knex.fn.now());
        table.timestamp("updated_at").defaultTo(knex.fn.now());
    });
}

export async function down(knex: Knex): Promise<void> {
    return await knex.schema.dropTable("contributors");
}
