import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable("companies", function (table) {
        table.increments("id").primary();
        table.string("business_name", 255).notNullable();
        table.string("industry", 255).notNullable();
        table.string("catch_phrase", 255);
        table.string("bs_company_statement", 255);
        table.string("logo", 255);
        table.string("type", 255);
        table.string("phone_number", 255);
        table.string("full_address", 255);
        table.string("latitude", 255);
        table.string("longitude", 255);
        table.string("suffix", 255);

        table.timestamp("created_at").defaultTo(knex.fn.now());
        table.timestamp("updated_at").defaultTo(knex.fn.now());
    });
}

export async function down(knex: Knex): Promise<void> {
    return await knex.schema.dropTable("companies");
}
