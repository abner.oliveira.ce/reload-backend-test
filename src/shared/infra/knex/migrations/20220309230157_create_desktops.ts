import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    return await knex.schema.createTable("desktops", function (table) {
        table.increments("id").primary();
        table
            .integer("company_id")
            .unsigned()
            .index()
            .references("id")
            .inTable("companies");
        table.string("platform", 255).notNullable();
        table.string("type", 255).notNullable();
        table.string("os", 255);
        table.string("ip", 255);
        table.timestamp("created_at").defaultTo(knex.fn.now());
        table.timestamp("updated_at").defaultTo(knex.fn.now());
    });
}

export async function down(knex: Knex): Promise<void> {
    return await knex.schema.dropTable("desktops");
}
