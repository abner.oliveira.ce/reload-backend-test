const knexfile = {
    development: {
        client: "mysql2",
        connection: {
            // host: "db",
            database: process.env.DB_NAME || "reload_test",
            user: process.env.DB_USER || "root",
            password: process.env.DB_PASS || "1234",
        },
        migrations: {
            tableName: "knex_migrations",
            directory: "migrations",
        },
        seeds: {
            directory: "seeds",
        },
    },
    migrations: {
        client: "mysql2",
        connection: {
            database: process.env.DB_NAME || "reload_test",
            user: process.env.DB_USER || "root",
            password: process.env.DB_PASS || "1234",
        },
        migrations: {
            tableName: "knex_migrations",
            directory: "migrations",
        },
        seeds: {
            directory: "seeds",
        },
    },
};

export default knexfile;
