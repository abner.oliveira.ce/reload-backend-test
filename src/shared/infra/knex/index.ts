import knex from "knex";

import knexfile from "./knexfile";

const connection = knex(knexfile.development);
// export const connMigrations = knex(knexfile.migrations);

export default connection;
