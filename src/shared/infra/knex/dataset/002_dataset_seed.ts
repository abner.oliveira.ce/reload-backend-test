import fs from "fs";
import { Knex } from "knex";

import { dataset } from "../dataset/datasetTS";

type Desktop = {
    id?: number;
    company_id?: number;
    platform: string;
    type: string;
    os: string;
    ip: string;
};

type Contributor = {
    id?: number;
    company_id?: number;
    firstName: string;
    lastName: string;
    title: string;
    jobTitle: string;
    age: number;
};

type Company = {
    id: number;
    business_name: string;
    suffix: string;
    industry: string;
    catch_phrase: string;
    bs_company_statement: string;
    logo: string;
    type: string;
    phone_number: string;
    full_address: string;
    latitude: string;
    longitude: string;
    contributors?: Contributor[];
    desktops?: Desktop[];
};

export async function seed(knex: Knex): Promise<void> {
    const companies: Company[] = [];
    const contributors: Contributor[] = [];
    const desktops: Desktop[] = [];

    await knex("contributors").del();
    await knex("desktops").del();
    await knex("companies").del();

    console.log("Deletando base!");

    dataset.forEach(async (v) => {
        const company: Company = {
            id: v.id,
            business_name: v.business_name,
            suffix: v.suffix,
            industry: v.industry,
            catch_phrase: v.catch_phrase,
            bs_company_statement: v.bs_company_statement,
            logo: v.logo,
            type: v.type,
            phone_number: v.phone_number,
            full_address: v.full_address,
            latitude: v.latitude,
            longitude: v.longitude,
        };
        const checkId = companies.find((el) => el.id === v.id);
        if (!checkId) {
            companies.push(company);
            console.log("ok");
        }

        v.contributors.forEach((c) => {
            const contributor: Contributor = {
                company_id: v.id,
                firstName: c.firstName,
                lastName: c.lastName,
                title: c.title,
                jobTitle: c.title,
                age: c.age,
            };
            contributors.push(contributor);
        });

        v.desktops.forEach((d) => {
            const desktop: Desktop = {
                company_id: v.id,
                platform: d.platform,
                type: d.type,
                os: d.os,
                ip: d.ip,
            };

            const checkId = desktops.find((el) => el.id === v.id);
            if (!checkId) {
                desktops.push(desktop);
                console.log("ok");
            }
        });
    });

    fs.writeFileSync("companies.json", JSON.stringify(companies));
    fs.writeFileSync("contributors.json", JSON.stringify(contributors));
    fs.writeFileSync("desktops.json", JSON.stringify(desktops));

    /* function companyPaginator(array: Company[], step: number) {
        const ids = [];
        for (let i = 0; i < array.length; i += step) {
            ids.push(array.slice(i, i + step));
        }
        return ids;
    }

    companyPaginator(companies, 100).forEach((sliced) => {
        fs.writeFileSync(`company${sliced[0].id}.json`, JSON.stringify(sliced));
        console.log("O arquivo foi criado!");
    });

    function cPaginator(array: Contributor[], step: number) {
        const ids = [];
        for (let i = 0; i < array.length; i += step) {
            ids.push(array.slice(i, i + step));
        }
        return ids;
    }

    cPaginator(contributors, 100).forEach((sliced) => {
        fs.writeFileSync(
            `contributor${sliced[0].company_id}.json`,
            JSON.stringify(sliced)
        );
        console.log("O arquivo foi criado!");
    });

    function dPaginator(array: Desktop[], step: number) {
        const ids = [];
        for (let i = 0; i < array.length; i += step) {
            ids.push(array.slice(i, i + step));
        }
        return ids;
    }

    dPaginator(desktops, 100).forEach((sliced) => {
        fs.writeFileSync(
            `desktop${sliced[0].company_id}.json`,
            JSON.stringify(sliced)
        );
        console.log("O arquivo foi criado!");
    });
 */
    /* await knex("companies").insert(companies);
    await knex("contributors").insert(contributors);
    await knex("desktops").insert(desktops);
 */
    /* console.log("companies", companies);
    console.log("contributors", contributors);
    console.log("desktops", desktops);
 */
    /* function companyPaginator(array: Company[], step: number) {
        const ids = [];
        for (let i = 0; i < array.length; i += step) {
            ids.push(array.slice(i, i + step));
        }
        return ids;
    }
    function contributorPaginator(array: Contributor[], step: number) {
        const ids = [];
        for (let i = 0; i < array.length; i += step) {
            ids.push(array.slice(i, i + step));
        }
        return ids;
    }
    function desktopPaginator(array: Desktop[], step: number) {
        const ids = [];
        for (let i = 0; i < array.length; i += step) {
            ids.push(array.slice(i, i + step));
        }
        return ids;
    }

    await knex("contributors").del();
    await knex("desktops").del();
    await knex("companies").del();

    companyPaginator(companies, 100).forEach(async (sliced) => {
        await knex("companies").insert(sliced);
    });

    desktopPaginator(desktops, 100).forEach(async (sliced) => {
        await knex("desktops").insert(sliced);
    });
    contributorPaginator(contributors, 100).forEach(async (sliced) => {
        await knex("contributors").insert(sliced);
    }); */
}
