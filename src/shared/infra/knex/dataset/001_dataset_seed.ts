import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries

    await knex("companies").del();
    await knex("desktops").del();
    await knex("contributors").del();

    // Inserts seed entries

    await knex("companies").insert([
        {
            id: 40068,
            business_name: "Schuppe - Spencer",
            suffix: "LLC",
            industry: "Tactics",
            catch_phrase: "Compatible background benchmark",
            bs_company_statement: "grow global infrastructures",
            logo: "http://placeimg.com/640/480/business",
            type: "enable",
            phone_number: "803.658.4521",
            full_address: "2037 Champlin Summit",
            latitude: "-73.1783",
            longitude: "55.6623",
        },
        {
            id: 1787,
            business_name: "Cormier, Hills and O'Kon",
            suffix: "and Sons",
            industry: "Infrastructure",
            catch_phrase: "Face to face discrete implementation",
            bs_company_statement: "transition front-end schemas",
            logo: "http://placeimg.com/640/480/business",
            type: "brand",
            phone_number: "1-550-860-5001",
            full_address: "77973 Roberta Mews",
            latitude: "-8.2864",
            longitude: "-63.0891",
        },
    ]);
}
