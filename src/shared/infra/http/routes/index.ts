import { Router } from "express";

import companyRouter from "../../../../modules/companies/infra/http/routes/company.routes";
import contributorRouter from "../../../../modules/contributors/infra/http/routes/contributors.routes";
import desktopsRouter from "../../../../modules/desktops/infra/http/routes/desktops.routes";

const routes = Router();

routes.use("/companies", companyRouter);
routes.use("/contributors", contributorRouter);
routes.use("/desktops", desktopsRouter);

export default routes;
