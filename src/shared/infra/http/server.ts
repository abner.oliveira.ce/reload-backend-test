import "reflect-metadata";
import "dotenv/config";
import * as Sentry from "@sentry/node";
import * as Tracing from "@sentry/tracing";
import { errors } from "celebrate";
import express, { NextFunction, Request, Response } from "express";
import swaggerUi from "swagger-ui-express";

import swaggerFile from "../../../swagger.json";
import AppError from "../../errors/AppError";
import "express-async-errors";
import routes from "./routes";

import "../knex";
import "../../container";

const app = express();

Sentry.init({
    dsn:
        process.env.DSN_SENTRY ||
        "https://c3dfefce359b4f0da77ed122439f59b2@o1165762.ingest.sentry.io/6255937",
    integrations: [
        new Sentry.Integrations.Http({ tracing: true }),
        new Tracing.Integrations.Express({ app }),
    ],
    tracesSampleRate: 1.0,
});
app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.tracingHandler());
app.use(express.json());

app.use(routes);
app.use(errors());

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerFile));
app.get("/debug-sentry", function mainHandler(req: Request, res: Response) {
    throw new Error("Test Sentry!");
});

app.use(Sentry.Handlers.errorHandler());

app.use((err: Error, request: Request, response: Response, _: NextFunction) => {
    if (err instanceof AppError) {
        return response.status(err.statusCode).json({
            status: "error",
            message: err.message,
        });
    }
    console.error(err);
    return response.status(500).json({
        status: "error",
        message: "Internal server error",
    });
});

app.listen(5000, () => {
    console.log("🚀 Server started on port 5000!");
});
