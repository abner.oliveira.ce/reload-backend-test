import { container } from "tsyringe";

import "./providers/CacheProvider";

import CompaniesRepository from "../../modules/companies/repositories/CompaniesRepository";
import ICompaniesRepository from "../../modules/companies/repositories/ICompaniesRepository";
import ContributorsRepository from "../../modules/contributors/repositories/ContributorsRepository";
import IContributorsRepository from "../../modules/contributors/repositories/IContributorsRepository";
import DesktopsRepository from "../../modules/desktops/repositories/DesktopsRepository";
import IDesktopsRepository from "../../modules/desktops/repositories/IDesktopsRepository";

container.registerSingleton<ICompaniesRepository>(
    "CompaniesRepository",
    CompaniesRepository
);
container.registerSingleton<IContributorsRepository>(
    "ContributorsRepository",
    ContributorsRepository
);

container.registerSingleton<IDesktopsRepository>(
    "DesktopsRepository",
    DesktopsRepository
);
