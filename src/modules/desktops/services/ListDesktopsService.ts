import { classToClass } from "class-transformer";
import { injectable, inject } from "tsyringe";

import { Desktop } from "../../../shared/@types";
import ICacheProvider from "../../../shared/container/providers/CacheProvider/models/ICacheProvider";
import IDesktopsRepository from "../repositories/IDesktopsRepository";

@injectable()
class ListDesktopsService {
    constructor(
        @inject("DesktopsRepository")
        private desktopsRepository: IDesktopsRepository,

        @inject("CacheProvider")
        private cacheProvider: ICacheProvider
    ) {}

    public async execute(): Promise<Desktop[]> {
        let desktops = await this.cacheProvider.recover<Desktop[]>(
            "desktops-list"
        );

        if (!desktops) {
            desktops = await this.desktopsRepository.findAll();

            await this.cacheProvider.save(
                `desktops-list`,
                classToClass(desktops)
            );
        }

        return desktops;
    }
}

export default ListDesktopsService;
