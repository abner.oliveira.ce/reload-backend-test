import { injectable, inject } from "tsyringe";

import { Desktop } from "../../../shared/@types";
import AppError from "../../../shared/errors/AppError";
import IDesktopsRepository from "../repositories/IDesktopsRepository";

interface IRequest {
    company_id: string;
}

@injectable()
class ListByCompanyDesktopService {
    constructor(
        @inject("DesktopsRepository")
        private desktopsRepository: IDesktopsRepository
    ) {}

    public async execute({ company_id }: IRequest): Promise<Desktop[] | []> {
        const desktop = await this.desktopsRepository.findByCompanyId(
            company_id
        );
        if (!desktop) {
            throw new AppError("Company not found.");
        }

        return desktop;
    }
}

export default ListByCompanyDesktopService;
