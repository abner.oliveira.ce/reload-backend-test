import { injectable, inject } from "tsyringe";

import { Desktop } from "../../../shared/@types";
import ICacheProvider from "../../../shared/container/providers/CacheProvider/models/ICacheProvider";
import IDesktopsRepository from "../repositories/IDesktopsRepository";

interface IRequest {
    company_id?: number;
    platform: string;
    type: string;
    os: string;
    ip: string;
}

@injectable()
class CreateDesktopService {
    constructor(
        @inject("DesktopsRepository")
        private desktopsRepository: IDesktopsRepository,

        @inject("CacheProvider")
        private cacheProvider: ICacheProvider
    ) {}

    public async execute({
        company_id,
        platform,
        type,
        os,
        ip,
    }: IRequest): Promise<Desktop> {
        const desktop = await this.desktopsRepository.create({
            company_id,
            platform,
            type,
            os,
            ip,
        });

        await this.cacheProvider.invalidatePrefix("desktops-list");

        return desktop;
    }
}

export default CreateDesktopService;
