import { injectable, inject } from "tsyringe";

import AppError from "../../../shared/errors/AppError";
import IDesktopsRepository from "../repositories/IDesktopsRepository";

interface IRequest {
    id: string;
}

@injectable()
class DeleteDesktopService {
    constructor(
        @inject("DesktopsRepository")
        private desktopsRepository: IDesktopsRepository
    ) {}

    public async execute({ id }: IRequest): Promise<void> {
        const data = await this.desktopsRepository.findById(id);
        if (!data) {
            throw new AppError("Desktop not found.");
        }

        await this.desktopsRepository.delete(id);
    }
}

export default DeleteDesktopService;
