import { injectable, inject } from "tsyringe";

import { Desktop } from "../../../shared/@types";
import ICacheProvider from "../../../shared/container/providers/CacheProvider/models/ICacheProvider";
import AppError from "../../../shared/errors/AppError";
import IDesktopsRepository from "../repositories/IDesktopsRepository";

interface IRequest {
    id: string;
    company_id?: number;
    platform: string;
    type: string;
    os: string;
    ip: string;
}

@injectable()
class CreateDesktopService {
    constructor(
        @inject("DesktopsRepository")
        private desktopsRepository: IDesktopsRepository,

        @inject("CacheProvider")
        private cacheProvider: ICacheProvider
    ) {}

    public async execute({
        id,
        company_id,
        platform,
        type,
        os,
        ip,
    }: IRequest): Promise<Desktop> {
        const checkExists = await this.desktopsRepository.findById(id);

        if (!checkExists) {
            throw new AppError("Desktop not found.");
        }
        const data = {
            company_id,
            platform,
            type,
            os,
            ip,
        };
        const desktop_id = id;

        const desktop = await this.desktopsRepository.update(data, desktop_id);

        await this.cacheProvider.invalidatePrefix("desktops-list");

        return desktop;
    }
}

export default CreateDesktopService;
