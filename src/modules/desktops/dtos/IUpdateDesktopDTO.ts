export default interface IUpdateDesktopDTO {
    company_id?: number;
    platform: string;
    type: string;
    os: string;
    ip: string;
}
