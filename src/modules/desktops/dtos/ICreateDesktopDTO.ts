export default interface ICreateDesktopDTO {
    company_id?: number;
    platform: string;
    type: string;
    os: string;
    ip: string;
}
