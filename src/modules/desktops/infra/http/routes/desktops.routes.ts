import express from "express";

import DesktopsController from "../controllers/DesktopsController";

const desktopRouter = express.Router();

desktopRouter.get("/", DesktopsController.index);
desktopRouter.get("/:company_id/company", DesktopsController.listByCompany);
desktopRouter.post("/", DesktopsController.store);
desktopRouter.patch("/:id", DesktopsController.update);
desktopRouter.delete("/:id", DesktopsController.delete);

export default desktopRouter;
