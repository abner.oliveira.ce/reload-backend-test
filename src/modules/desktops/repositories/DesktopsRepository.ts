// import { getRepository, Repository, Raw } from "typeorm";

import { Desktop } from "../../../shared/@types";
import connection from "../../../shared/infra/knex";
import ICreateDesktopDTO from "../dtos/ICreateDesktopDTO";
import IUpdateDesktopDTO from "../dtos/IUpdateDesktopDTO";
import IDesktopsRepository from "./IDesktopsRepository";

class DesktopsRepository implements IDesktopsRepository {
    public async create(desktopData: ICreateDesktopDTO): Promise<Desktop> {
        const created = await connection("desktops").insert(desktopData);
        const Desktop = await connection("desktops").where("id", created);
        return Desktop[0];
    }

    public async update(
        desktopData: IUpdateDesktopDTO,
        desktop_id: string
    ): Promise<Desktop> {
        await connection("desktops")
            .update(desktopData)
            .where("id", desktop_id);
        const desktop = await connection("desktops").where("id", desktop_id);
        return desktop[0];
    }

    public async findAll(): Promise<Desktop[] | []> {
        const findDesktops = await connection("desktops");
        return findDesktops;
    }

    public async findById(id: string): Promise<Desktop | undefined> {
        const desktop = await connection("desktops").where("id", id);
        return desktop[0];
    }

    public async findByCompanyId(company_id: string): Promise<Desktop[] | []> {
        const desktops = await connection("desktops").where(
            " company_id",
            company_id
        );
        return desktops;
    }

    public async delete(id: string): Promise<void> {
        await connection("desktops").del().where("id", id);
    }
}

export default DesktopsRepository;
