import { Desktop } from "../../../shared/@types";
import ICreateDesktopDTO from "../dtos/ICreateDesktopDTO";

export default interface IDesktopsRepository {
    create(data: ICreateDesktopDTO): Promise<Desktop>;
    update(data: ICreateDesktopDTO, contributor_id: string): Promise<Desktop>;
    findAll(): Promise<Desktop[] | []>;
    findById(id: string): Promise<Desktop | undefined>;
    findByCompanyId(company_id: string): Promise<Desktop[] | []>;
    delete(id: string): void;
}
