import { classToClass } from "class-transformer";
import { NextFunction, Request, Response } from "express";
import { container } from "tsyringe";

import CreateCompanyService from "../../../services/CreateCompanyService";
import DeleteCompanyService from "../../../services/DeleteCompanyService";
import ListCompaniesService from "../../../services/ListCompaniesService";
import ShowByNameCompanyService from "../../../services/ShowByNameCompanyService";
import ShowCompanyService from "../../../services/ShowCompanyService";
import UpdateCompanyService from "../../../services/UpdateCompanyService";

class CompanyController {
    public async index(
        request: Request,
        response: Response
    ): Promise<Response> {
        const listCompanies = container.resolve(ListCompaniesService);
        const companies = await listCompanies.execute();
        return response.status(200).json(classToClass(companies));
    }

    public async show(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;

        const currentCompany = container.resolve(ShowCompanyService);
        const company = await currentCompany.execute({ id });
        return response.status(200).json(classToClass(company));
    }

    public async showByName(
        request: Request,
        response: Response
    ): Promise<Response> {
        const { name } = request.params;

        const currentCompany = container.resolve(ShowByNameCompanyService);
        const company = await currentCompany.execute({ name });
        return response.status(200).json(classToClass(company));
    }
    public async store(
        request: Request,
        response: Response,
        next: NextFunction
    ): Promise<Response> {
        const {
            business_name,
            suffix,
            industry,
            catch_phrase,
            bs_company_statement,
            logo,
            type,
            phone_number,
            full_address,
            latitude,
            longitude,
        } = request.body;

        const createCompany = container.resolve(CreateCompanyService);
        const company = await createCompany.execute({
            business_name,
            suffix,
            industry,
            catch_phrase,
            bs_company_statement,
            logo,
            type,
            phone_number,
            full_address,
            latitude,
            longitude,
        });
        return response.status(200).json(classToClass(company));
    }
    public async update(
        request: Request,
        response: Response,
        next: NextFunction
    ): Promise<Response> {
        const { id } = request.params;
        const {
            business_name,
            suffix,
            industry,
            catch_phrase,
            bs_company_statement,
            logo,
            type,
            phone_number,
            full_address,
            latitude,
            longitude,
        } = request.body;
        const updateCompany = container.resolve(UpdateCompanyService);
        const company = await updateCompany.execute({
            id,
            business_name,
            suffix,
            industry,
            catch_phrase,
            bs_company_statement,
            logo,
            type,
            phone_number,
            full_address,
            latitude,
            longitude,
        });
        return response.status(200).json(classToClass(company));
    }

    public async delete(
        request: Request,
        response: Response,
        next: NextFunction
    ): Promise<Response> {
        const { id } = request.params;

        const currentCompany = container.resolve(DeleteCompanyService);
        await currentCompany.execute({ id });
        return response.status(200).send();
    }
}

export default new CompanyController();
