import express from "express";

import CompanyController from "../controllers/CompanyController";

const companyRouter = express.Router();

companyRouter.get("/", CompanyController.index);
companyRouter.get("/:id", CompanyController.show);
companyRouter.get("/:name/name", CompanyController.showByName);
companyRouter.post("/", CompanyController.store);
companyRouter.patch("/:id", CompanyController.update);
companyRouter.delete("/:id", CompanyController.delete);

export default companyRouter;
