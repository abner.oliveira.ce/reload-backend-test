// import { getRepository, Repository, Raw } from "typeorm";

import { Company } from "../../../../shared/@types";
import ICreateCompanyDTO from "../../dtos/ICreateCompanyDTO";
import IUpdateCompanyDTO from "../../dtos/IUpdateCompanyDTO";
import ICompaniesRepository from "../ICompaniesRepository";

class FakeCompaniesRepository implements ICompaniesRepository {
    private companies: Company[] = [];

    public async create(companyData: ICreateCompanyDTO) {
        const company = {
            id: this.companies.length + 1,
            business_name: companyData.business_name,
            suffix: companyData.suffix,
            industry: companyData.industry,
            catch_phrase: companyData.catch_phrase,
            bs_company_statement: companyData.bs_company_statement,
            logo: companyData.logo,
            type: companyData.type,
            phone_number: companyData.phone_number,
            full_address: companyData.full_address,
            latitude: companyData.latitude,
            longitude: companyData.longitude,
        };

        this.companies.push(company);

        return company;
    }

    public async update(companyData: IUpdateCompanyDTO, company_id: string) {
        const dataIndex = this.companies.findIndex(
            (elIndex) => elIndex.id === companyData.id
        );
        this.companies[dataIndex] = companyData;
        return companyData;
    }

    public async findAll() {
        return this.companies;
    }

    public async findById(id: string) {
        const data = this.companies.find((el) => el.id === parseInt(id, 10));
        return data;
    }

    public async delete(id: string) {
        /* const dataIndex = this.companies.findIndex(
            (elIndex) => elIndex.id === parseInt(id, 10)
        ); */
        this.companies = this.companies.filter(
            (item) => item.id !== parseInt(id, 10)
        );
    }

    public async findByName(name: string) {
        const data = this.companies.find((el) => el.business_name === name);
        return data;
    }
}

export default FakeCompaniesRepository;
