// import { getRepository, Repository, Raw } from "typeorm";

import { Company } from "../../../shared/@types";
import connection from "../../../shared/infra/knex";
import ICreateCompanyDTO from "../dtos/ICreateCompanyDTO";
import IUpdateCompanyDTO from "../dtos/IUpdateCompanyDTO";
import ICompaniesRepository from "./ICompaniesRepository";

class CompaniesRepository implements ICompaniesRepository {
    public async create(companyData: ICreateCompanyDTO): Promise<Company> {
        const created = await connection("companies").insert(companyData);
        const company = await connection("companies").where("id", created);
        return company[0];
    }

    public async update(
        companyData: IUpdateCompanyDTO,
        company_id: string
    ): Promise<Company> {
        await connection("companies")
            .update(companyData)
            .where("id", company_id);
        const company = await connection("companies").where("id", company_id);
        return company[0];
    }

    public async findAll(): Promise<Company[] | []> {
        const findCompanies = await connection("companies");
        return findCompanies;
    }

    public async findById(id: string): Promise<Company | undefined> {
        const company = await connection("companies").where("id", id);
        return company[0];
    }

    public async delete(id: string): Promise<void> {
        await connection("companies").del().where("id", id);
        // return company[0];
    }

    public async findByName(name: string): Promise<Company | undefined> {
        const company: any = await connection("companies").whereILike(
            "business_name",
            `%${name}%`
        );

        return company[0];
    }
}

export default CompaniesRepository;
