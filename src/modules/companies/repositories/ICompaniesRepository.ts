import { Company } from "../../../shared/@types";
import ICreateCompanyDTO from "../dtos/ICreateCompanyDTO";
import IUpdateCompanyDTO from "../dtos/IUpdateCompanyDTO";

export default interface ICompaniesRepository {
    create(data: ICreateCompanyDTO): Promise<Company>;
    update(data: IUpdateCompanyDTO, company_id: string): Promise<Company>;
    findAll(): Promise<Company[] | []>;
    findById(id: string): Promise<Company | undefined>;
    delete(id: string): void;
    findByName(name: string): Promise<Company | undefined>;
}
