import FakeCacheProvider from "../../../shared/container/providers/CacheProvider/fakes/FakeCacheProvider";
import AppError from "../../../shared/errors/AppError";
import FakeCompaniesRepository from "../repositories/Fakes/FakeCompaniesRepository";
import CreateCompanyService from "./CreateCompanyService";

let fakeCompaniesRepository: FakeCompaniesRepository;
let fakeCacheProvider: FakeCacheProvider;
let createCompany: CreateCompanyService;

describe("CreateCompany", () => {
    beforeEach(() => {
        fakeCompaniesRepository = new FakeCompaniesRepository();
        fakeCacheProvider = new FakeCacheProvider();

        createCompany = new CreateCompanyService(
            fakeCompaniesRepository,
            fakeCacheProvider
        );
    });

    it("should be able to create a new company", async () => {
        const data = await createCompany.execute({
            business_name: "Rippin - Stamm",
            suffix: "Group",
            industry: "Identity",
            catch_phrase: "Streamlined content-based neural-net",
            bs_company_statement: "embrace open-source e-business",
            logo: "http://placeimg.com/640/480/business",
            type: "disintermediate",
            phone_number: "(273) 213-6293",
            full_address: "269 Martina Greens",
            latitude: "-75.6272",
            longitude: "167.8590",
        });

        expect(data).toHaveProperty("id");
    });
});
