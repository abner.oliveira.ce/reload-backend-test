import { injectable, inject } from "tsyringe";

import { Company } from "../../../shared/@types";
import ICacheProvider from "../../../shared/container/providers/CacheProvider/models/ICacheProvider";
import AppError from "../../../shared/errors/AppError";
import ICompaniesRepository from "../repositories/ICompaniesRepository";

interface IRequest {
    id: string;
    business_name: string;
    suffix: string;
    industry: string;
    catch_phrase: string;
    bs_company_statement: string;
    logo: string;
    type: string;
    phone_number: string;
    full_address: string;
    latitude: string;
    longitude: string;
}

@injectable()
class CreateCompanyService {
    constructor(
        @inject("CompaniesRepository")
        private companiesRepository: ICompaniesRepository,

        @inject("CacheProvider")
        private cacheProvider: ICacheProvider
    ) {}

    public async execute({
        id,
        business_name,
        suffix,
        industry,
        catch_phrase,
        bs_company_statement,
        logo,
        type,
        phone_number,
        full_address,
        latitude,
        longitude,
    }: IRequest): Promise<Company> {
        const checkCompanyExists = await this.companiesRepository.findById(id);

        if (!checkCompanyExists) {
            throw new AppError("Company not found.");
        }
        const data = {
            business_name,
            suffix,
            industry,
            catch_phrase,
            bs_company_statement,
            logo,
            type,
            phone_number,
            full_address,
            latitude,
            longitude,
        };
        const company_id = id;

        const company = await this.companiesRepository.update(data, company_id);

        await this.cacheProvider.invalidatePrefix("companies-list");

        return company;
    }
}

export default CreateCompanyService;
