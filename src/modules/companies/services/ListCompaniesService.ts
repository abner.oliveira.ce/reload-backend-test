import { classToClass } from "class-transformer";
import { injectable, inject } from "tsyringe";

import { Company } from "../../../shared/@types";
import ICacheProvider from "../../../shared/container/providers/CacheProvider/models/ICacheProvider";
import ICompaniesRepository from "../repositories/ICompaniesRepository";

@injectable()
class ListCompaniesService {
    constructor(
        @inject("CompaniesRepository")
        private companiesRepository: ICompaniesRepository,

        @inject("CacheProvider")
        private cacheProvider: ICacheProvider
    ) {}

    public async execute(): Promise<Company[]> {
        let companies = await this.cacheProvider.recover<Company[]>(
            "companies-list"
        );

        if (!companies) {
            companies = await this.companiesRepository.findAll();

            await this.cacheProvider.save(
                `companies-list`,
                classToClass(companies)
            );
        }

        return companies;
    }
}

export default ListCompaniesService;
