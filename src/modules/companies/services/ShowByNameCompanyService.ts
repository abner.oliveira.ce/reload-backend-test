import { injectable, inject } from "tsyringe";

import { Company } from "../../../shared/@types";
import AppError from "../../../shared/errors/AppError";
import ICompaniesRepository from "../repositories/ICompaniesRepository";

interface IRequest {
    name: string;
}

@injectable()
class ShowCompanyService {
    constructor(
        @inject("CompaniesRepository")
        private companiesRepository: ICompaniesRepository
    ) {}

    public async execute({ name }: IRequest): Promise<Company | undefined> {
        const company = await this.companiesRepository.findByName(name);

        if (!company) {
            throw new AppError("Company not found.");
        }

        return company;
    }
}

export default ShowCompanyService;
