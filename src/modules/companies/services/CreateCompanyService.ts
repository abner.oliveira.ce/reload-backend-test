import { injectable, inject } from "tsyringe";

import { Company } from "../../../shared/@types";
import ICacheProvider from "../../../shared/container/providers/CacheProvider/models/ICacheProvider";
// import AppError from "../../../shared/errors/AppError";
import ICompaniesRepository from "../repositories/ICompaniesRepository";

interface IRequest {
    business_name: string;
    suffix: string;
    industry: string;
    catch_phrase: string;
    bs_company_statement: string;
    logo: string;
    type: string;
    phone_number: string;
    full_address: string;
    latitude: string;
    longitude: string;
}

@injectable()
class CreateCompanyService {
    constructor(
        @inject("CompaniesRepository")
        private companiesRepository: ICompaniesRepository,

        @inject("CacheProvider")
        private cacheProvider: ICacheProvider
    ) {}

    public async execute({
        business_name,
        suffix,
        industry,
        catch_phrase,
        bs_company_statement,
        logo,
        type,
        phone_number,
        full_address,
        latitude,
        longitude,
    }: IRequest): Promise<Company> {
        const company = await this.companiesRepository.create({
            business_name,
            suffix,
            industry,
            catch_phrase,
            bs_company_statement,
            logo,
            type,
            phone_number,
            full_address,
            latitude,
            longitude,
        });

        await this.cacheProvider.invalidatePrefix("companies-list");

        return company;
    }
}

export default CreateCompanyService;
