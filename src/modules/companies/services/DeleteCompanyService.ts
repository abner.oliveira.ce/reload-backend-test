import { injectable, inject } from "tsyringe";

import AppError from "../../../shared/errors/AppError";
import ICompaniesRepository from "../repositories/ICompaniesRepository";

interface IRequest {
    id: string;
}

@injectable()
class DeleteCompanyService {
    constructor(
        @inject("CompaniesRepository")
        private companiesRepository: ICompaniesRepository
    ) {}

    public async execute({ id }: IRequest): Promise<void> {
        const company = await this.companiesRepository.findById(id);
        if (!company) {
            throw new AppError("Company not found.");
        }

        await this.companiesRepository.delete(id);
    }
}

export default DeleteCompanyService;
