import { injectable, inject } from "tsyringe";

import { Contributor } from "../../../shared/@types";
import ICacheProvider from "../../../shared/container/providers/CacheProvider/models/ICacheProvider";
import AppError from "../../../shared/errors/AppError";
import IContributorsRepository from "../repositories/IContributorsRepository";

interface IRequest {
    id: string;
    company_id: number;
    firstName: string;
    lastName: string;
    title: string;
    jobTitle: string;
    age: number;
}

@injectable()
class CreateContributorService {
    constructor(
        @inject("ContributorsRepository")
        private contributorsRepository: IContributorsRepository,

        @inject("CacheProvider")
        private cacheProvider: ICacheProvider
    ) {}

    public async execute({
        id,
        company_id,
        firstName,
        lastName,
        title,
        jobTitle,
        age,
    }: IRequest): Promise<Contributor> {
        const checkExists = await this.contributorsRepository.findById(id);

        if (!checkExists) {
            throw new AppError("Contributor not found.");
        }
        const data = {
            company_id,
            firstName,
            lastName,
            title,
            jobTitle,
            age,
        };
        const contributor_id = id;

        const contributor = await this.contributorsRepository.update(
            data,
            contributor_id
        );

        await this.cacheProvider.invalidatePrefix("contributors-list");

        return contributor;
    }
}

export default CreateContributorService;
