import { classToClass } from "class-transformer";
import { injectable, inject } from "tsyringe";

import { Contributor } from "../../../shared/@types";
import ICacheProvider from "../../../shared/container/providers/CacheProvider/models/ICacheProvider";
import IContributorsRepository from "../repositories/IContributorsRepository";

@injectable()
class ListContributorsService {
    constructor(
        @inject("ContributorsRepository")
        private contributorsRepository: IContributorsRepository,

        @inject("CacheProvider")
        private cacheProvider: ICacheProvider
    ) {}

    public async execute(): Promise<Contributor[]> {
        let contributors = await this.cacheProvider.recover<Contributor[]>(
            "contributors-list"
        );

        if (!contributors) {
            contributors = await this.contributorsRepository.findAll();

            await this.cacheProvider.save(
                `Contributors-list`,
                classToClass(contributors)
            );
        }

        return contributors;
    }
}

export default ListContributorsService;
