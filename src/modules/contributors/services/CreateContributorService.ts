import { injectable, inject } from "tsyringe";

import { Contributor } from "../../../shared/@types";
import ICacheProvider from "../../../shared/container/providers/CacheProvider/models/ICacheProvider";
import IContributorsRepository from "../repositories/IContributorsRepository";

interface IRequest {
    company_id: number;
    firstName: string;
    lastName: string;
    title: string;
    jobTitle: string;
    age: number;
}

@injectable()
class CreateContributorService {
    constructor(
        @inject("ContributorsRepository")
        private contributorsRepository: IContributorsRepository,

        @inject("CacheProvider")
        private cacheProvider: ICacheProvider
    ) {}

    public async execute({
        company_id,
        firstName,
        lastName,
        title,
        jobTitle,
        age,
    }: IRequest): Promise<Contributor> {
        const contributor = await this.contributorsRepository.create({
            company_id,
            firstName,
            lastName,
            title,
            jobTitle,
            age,
        });

        await this.cacheProvider.invalidatePrefix("contributors-list");

        return contributor;
    }
}

export default CreateContributorService;
