import { injectable, inject } from "tsyringe";

import AppError from "../../../shared/errors/AppError";
import IContributorsRepository from "../repositories/IContributorsRepository";

interface IRequest {
    id: string;
}

@injectable()
class DeleteContributorService {
    constructor(
        @inject("ContributorsRepository")
        private contributorsRepository: IContributorsRepository
    ) {}

    public async execute({ id }: IRequest): Promise<void> {
        const data = await this.contributorsRepository.findById(id);
        if (!data) {
            throw new AppError("Contributor not found.");
        }

        await this.contributorsRepository.delete(id);
    }
}

export default DeleteContributorService;
