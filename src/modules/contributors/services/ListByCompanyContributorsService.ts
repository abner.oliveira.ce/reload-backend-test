import { injectable, inject } from "tsyringe";

import { Contributor } from "../../../shared/@types";
import AppError from "../../../shared/errors/AppError";
import IContributorsRepository from "../repositories/IContributorsRepository";

interface IRequest {
    company_id: string;
}

@injectable()
class ListByCompanyContributorsService {
    constructor(
        @inject("ContributorsRepository")
        private contributorsRepository: IContributorsRepository
    ) {}

    public async execute({
        company_id,
    }: IRequest): Promise<Contributor[] | []> {
        const contributor = await this.contributorsRepository.findByCompanyId(
            company_id
        );
        if (!contributor) {
            throw new AppError("Company not found.");
        }

        return contributor;
    }
}

export default ListByCompanyContributorsService;
