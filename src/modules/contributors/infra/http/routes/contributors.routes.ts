import express from "express";

import ContributorsController from "../controllers/ContributorsController";

const contributorRouter = express.Router();

contributorRouter.get("/", ContributorsController.index);
contributorRouter.get("/:company_id/company", ContributorsController.show);
contributorRouter.post("/", ContributorsController.store);
contributorRouter.patch("/:id", ContributorsController.update);
contributorRouter.delete("/:id", ContributorsController.delete);

export default contributorRouter;
