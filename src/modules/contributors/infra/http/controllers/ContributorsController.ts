import { classToClass } from "class-transformer";
import { NextFunction, Request, Response } from "express";
import { container } from "tsyringe";

import CreateContributorService from "../../../services/CreateContributorService";
import DeleteContributorService from "../../../services/DeleteContributorService";
import ListByCompanyContributorsService from "../../../services/ListByCompanyContributorsService";
import ListCompaniesService from "../../../services/ListContributorsService";
import UpdateContributorService from "../../../services/UpdateContributorsService";

class ContributorsController {
    public async index(
        request: Request,
        response: Response
    ): Promise<Response> {
        const list = container.resolve(ListCompaniesService);
        const data = await list.execute();
        return response.status(200).json(classToClass(data));
    }

    public async show(request: Request, response: Response): Promise<Response> {
        const { company_id } = request.params;

        const list = container.resolve(ListByCompanyContributorsService);
        const data = await list.execute({ company_id });
        return response.status(200).json(classToClass(data));
    }

    public async store(
        request: Request,
        response: Response,
        next: NextFunction
    ): Promise<Response> {
        const { company_id, firstName, lastName, title, jobTitle, age } =
            request.body;

        const createContributor = container.resolve(CreateContributorService);
        const data = await createContributor.execute({
            company_id,
            firstName,
            lastName,
            title,
            jobTitle,
            age,
        });
        return response.status(200).json(classToClass(data));
    }
    public async update(
        request: Request,
        response: Response,
        next: NextFunction
    ): Promise<Response> {
        const { id } = request.params;
        const { company_id, firstName, lastName, title, jobTitle, age } =
            request.body;
        const updateContributor = container.resolve(UpdateContributorService);
        const data = await updateContributor.execute({
            id,
            company_id,
            firstName,
            lastName,
            title,
            jobTitle,
            age,
        });
        return response.status(200).json(classToClass(data));
    }

    public async delete(
        request: Request,
        response: Response,
        next: NextFunction
    ): Promise<Response> {
        const { id } = request.params;

        const currentContributor = container.resolve(DeleteContributorService);
        await currentContributor.execute({ id });
        return response.status(200).send();
    }
}

export default new ContributorsController();
