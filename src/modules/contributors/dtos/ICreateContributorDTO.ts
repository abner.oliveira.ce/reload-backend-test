export default interface ICreateContributorDTO {
    company_id?: number;
    firstName: string;
    lastName: string;
    title: string;
    jobTitle: string;
    age: number;
}
