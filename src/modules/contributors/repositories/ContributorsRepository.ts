// import { getRepository, Repository, Raw } from "typeorm";

import { Contributor } from "../../../shared/@types";
import connection from "../../../shared/infra/knex";
import ICreateContributorDTO from "../dtos/ICreateContributorDTO";
import IUpdateContributorDTO from "../dtos/IUpdateContributorDTO";
import IContributorsRepository from "./IContributorsRepository";

class ContributorsRepository implements IContributorsRepository {
    public async create(
        contributorData: ICreateContributorDTO
    ): Promise<Contributor> {
        const created = await connection("contributors").insert(
            contributorData
        );
        const contributor = await connection("contributors").where(
            "id",
            created
        );
        return contributor[0];
    }

    public async update(
        contributorData: IUpdateContributorDTO,
        contributor_id: string
    ): Promise<Contributor> {
        await connection("contributors")
            .update(contributorData)
            .where("id", contributor_id);
        const contributor = await connection("contributors").where(
            "id",
            contributor_id
        );
        return contributor[0];
    }

    public async findAll(): Promise<Contributor[] | []> {
        const findContributors = await connection("contributors");
        return findContributors;
    }

    public async findById(id: string): Promise<Contributor | undefined> {
        const contributor = await connection("contributors").where("id", id);
        return contributor[0];
    }

    public async findByCompanyId(
        company_id: string
    ): Promise<Contributor[] | []> {
        const contributor = await connection("contributors").where(
            " company_id",
            company_id
        );
        return contributor;
    }

    public async delete(id: string): Promise<void> {
        await connection("contributors").del().where("id", id);
    }
}

export default ContributorsRepository;
