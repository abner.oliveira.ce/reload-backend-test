import { Contributor } from "../../../shared/@types";
import ICreateContributorDTO from "../dtos/ICreateContributorDTO";
import IUpdateContributorDTO from "../dtos/IUpdateContributorDTO";

export default interface IContributorsRepository {
    create(data: ICreateContributorDTO): Promise<Contributor>;
    update(
        data: IUpdateContributorDTO,
        contributor_id: string
    ): Promise<Contributor>;
    findAll(): Promise<Contributor[] | []>;
    findById(id: string): Promise<Contributor | undefined>;
    findByCompanyId(company_id: string): Promise<Contributor[] | []>;
    delete(id: string): void;
}
