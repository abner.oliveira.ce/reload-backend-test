import * as Sentry from "@sentry/node";
import * as Tracing from "@sentry/tracing";

export default {
    dsn: "https://c3dfefce359b4f0da77ed122439f59b2@o1165762.ingest.sentry.io/6255937",
    integrations: [
        // enable HTTP calls tracing
        new Sentry.Integrations.Http({ tracing: true }),
        // enable Express.js middleware tracing
        new Tracing.Integrations.Express({ app }),
    ],

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
};
