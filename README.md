# reload-backend-test

Teste de Backend para a empresa Reload.co

## Requisitos

- [X] NodeJs  14.x (TypeScript)
- [X] MySQL 5.7
- [ ] Restify (Usei o ExpressJs)
- [X] Knex
- [ ] Jest (Apenas iniciado)
- [X] Redis
- [X] Docker

## Requisitos Bônus

- [X] docker-compose
- [X] CI/CD
- [X] Sentry
- [X] Swagger
- [ ] Grafana
- [ ] Prometheus
- [ ] Terraform

## Explicações

- O projeto foi desenvolvido com node e TS. 
- Troquei o restify pelo epxress por ter mais familiaridade com esse framework. 
- Tenho costume de usar ORMs como Sequelize, TypeORM e Prisma, e Knex foi uma novidade
- Apesar da simplicidade do Knex para realizar consultas, inserts e updates tive alguns problemas na criação dos seeds
- Fiz o tratamento de todo o dataset, retirando as tuplas com Ids duplicados, bem como a criação dos PKs de contributors e as FKs das tabelas dependentes.
- Build - está sendo gerado na pasta dist e pode ser acessado pelo script yarn start:prod
- Build - esta apresentando erro na conversão pelo Babel. Tudo isso impacta no processo de deploy em js. O CI funciona, mas a aplicação não roda.
- Docker - Gerando a imagem - docker build -t reload-node .   
- Docker - Subindo container - docker run -p 5000:3333 --name reload-api reload-api
- docker-compose - problema de comunicação com os bancos de dados seja em rede própria ou na bridge. Vários testes realizados com network_mode e links
- docker-compose - a fim de subir uma versão para avaliação retirei o service app do docker-compose.yml, sendo assim a API só irá rodar local a partir do commando yarn start ou yarn dev:server.
- Swagger - faltou a definição dos route params
- Sentry - configuração feita podendo ser testada com variável de ambiente DSN_SENTRY. Apresenta debug na rota /debug-sentry
- CI/CD - Configurado o build, docker-build e inserção da imagem no registry.gitlab.com
- CI/CD - Deploy na GCP não concluído

## Como usar 

```
# clonar repositório
git clone https://gitlab.com/abner.oliveira.ce/reload-backend-test.git
# instalar dependencias
yarn
# Subir os container dos bancos
docker-compose up -d
# Migrations e Seeds
yarn migrations
yarn seeds
# Rodar aplicação no modo dev
yarn dev:server ou yarn start
```



