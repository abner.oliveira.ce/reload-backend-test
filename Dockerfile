FROM node:14-alpine

WORKDIR /usr/app
COPY package.json .
RUN yarn 
COPY . .
EXPOSE 3333
# RUN yarn migrations migrate:latest
# RUN yarn migrations seed:run  
CMD ["yarn", "dev:server"]


